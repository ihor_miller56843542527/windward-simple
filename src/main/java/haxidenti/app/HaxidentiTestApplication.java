package haxidenti.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class HaxidentiTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(HaxidentiTestApplication.class, args);
    }

}
