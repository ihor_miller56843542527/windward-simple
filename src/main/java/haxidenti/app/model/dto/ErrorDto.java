package haxidenti.app.model.dto;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ErrorDto {
    private String reason;
    private int status;

    public ErrorDto(String reason, HttpStatus status) {
        this.reason = reason;
        this.status = status.value();
    }
}
