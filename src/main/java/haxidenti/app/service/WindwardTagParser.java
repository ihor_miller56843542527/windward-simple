package haxidenti.app.service;

import haxidenti.app.exception.ProjectException;
import haxidenti.app.model.dto.TagDto;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class WindwardTagParser implements TagParserService {

    private static final Pattern PATTERN_TAG_XML_FINDER = Pattern.compile("AUTOTEXTLIST\\s.+?(select.+?nickname.+?)(?=/>)", Pattern.MULTILINE);
    private static final Pattern PATTERN_PROPERTY_FINDER = Pattern.compile("(.*?)='(.*?)'", Pattern.MULTILINE);

    private static final String KEY_DEFAULT_BODY = "default";
    private static final String KEY_NICKNAME = "nickname";

    private static final byte TAG_FINDER_MATCHER_GROUP = 1;
    private static final byte TAG_FINDER_MATCHER_SECOND_GROUP = 2;

    public List<TagDto> parseTemplate(InputStream docx) {
        AtomicInteger orderCount = new AtomicInteger();

        return getTagsFromDocxStream(docx).stream()
                .filter(tag -> tag.get(KEY_DEFAULT_BODY) != null)
                .map(tag -> {
                    int order = orderCount.getAndIncrement();
                    TagDto dto = new TagDto(filterTagName(tag.get(KEY_NICKNAME)), tag.get(KEY_DEFAULT_BODY), order);
                    return dto;
                })
                .collect(Collectors.toList());
    }

    private List<HashMap<String, String>> getTagsFromDocxStream(InputStream docxStream) {
        try {
            XWPFDocument document = new XWPFDocument(docxStream);
            String xml = document.getDocument().xmlText();
            document.close();
            return getTagsFromXMLString(xml);
        } catch (IOException e) {
            throw new ProjectException(e);
        }
    }

    private List<HashMap<String, String>> getTagsFromXMLString(String xml) {
        Matcher matcher = PATTERN_TAG_XML_FINDER.matcher(xml);
        List<HashMap<String, String>> tagList = new LinkedList<>();
        while (matcher.find()) {
            String foundTagInfo = matcher.group(TAG_FINDER_MATCHER_GROUP);
            HashMap<String, String> tag = parseTagString(foundTagInfo);
            tagList.add(tag);
        }
        return tagList;
    }

    private HashMap<String, String> parseTagString(String tagString) {
        Matcher matcher = PATTERN_PROPERTY_FINDER.matcher(tagString);
        HashMap<String, String> tag = new HashMap<>();
        while (matcher.find()) {
            String key = matcher.group(TAG_FINDER_MATCHER_GROUP).trim();
            String value = matcher.group(TAG_FINDER_MATCHER_SECOND_GROUP).trim();
            tag.put(key, value);
        }
        return tag;
    }

    private String filterTagName(String name) {
        if (name.startsWith("[") && name.endsWith("]")) {
            name = name.substring(1);
            name = name.substring(0, name.length() - 1);
        }
        return name;
    }

}
