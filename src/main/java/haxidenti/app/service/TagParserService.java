package haxidenti.app.service;

import haxidenti.app.model.dto.TagDto;

import java.io.InputStream;
import java.util.List;

public interface TagParserService {
    List<TagDto> parseTemplate(InputStream templateStream);
}
