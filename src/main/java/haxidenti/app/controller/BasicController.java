package haxidenti.app.controller;

import haxidenti.app.exception.ProjectException;
import haxidenti.app.model.dto.TagDto;
import haxidenti.app.service.TagParserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileInputStream;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class BasicController {

    private final TagParserService tagParser;

    @GetMapping
    public List<TagDto> getDocument() {
        try (FileInputStream file = new FileInputStream("template-to-parse.docx")) {
            return tagParser.parseTemplate(file);
        } catch (Exception e) {
            throw new ProjectException(e);
        }
    }
}
