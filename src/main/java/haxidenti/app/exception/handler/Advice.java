package haxidenti.app.exception.handler;

import haxidenti.app.model.dto.ErrorDto;
import haxidenti.app.exception.ProjectException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class Advice {

    @ExceptionHandler(ProjectException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto anyProjectError(ProjectException e) {
        return new ErrorDto("Project code error", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto anyError(Exception e) {
        return new ErrorDto("INTERNAL ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
