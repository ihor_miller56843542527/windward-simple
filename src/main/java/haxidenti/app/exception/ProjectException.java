package haxidenti.app.exception;

public class ProjectException extends RuntimeException {

    public ProjectException(Exception e) {
        super(e);
    }
}
